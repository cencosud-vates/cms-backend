package com.cencosud.sim.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cencosud.sim.model.request.CargaPcbsRequest;
import com.cencosud.sim.model.response.CargasPcbsResponse;
import com.cencosud.sim.service.CargaPcbsService;

@RestController
@RequestMapping("/rest")
public class CargaPcbsController {
	static final Logger log = Logger.getLogger(CargaPcbsController.class);
	
	@Autowired
	@Qualifier("CargaPcbsService")
	private CargaPcbsService cargaPcbsService;
		
	@CrossOrigin(origins = "*")
	@PostMapping("/cargar")
	public CargasPcbsResponse procesarCargaPcbs(@RequestBody CargaPcbsRequest cargaPcbsRequest) {
		log.info("INGRESA A ===> procesarCargaPcbs("+cargaPcbsRequest+")");
		return cargaPcbsService.procesarCargaPcbs(cargaPcbsRequest);
	}
	
	
}
