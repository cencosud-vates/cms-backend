package com.cencosud.sim.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cencosud.sim.model.TrackingModel;
import com.cencosud.sim.model.request.TrackingRequest;
import com.cencosud.sim.service.TrackingService;

@RestController
@RequestMapping("/rest")
public class GetTrackingController {
	static final Logger log = Logger.getLogger(GetTrackingController.class);
	
	@Autowired
	@Qualifier("TrackingService")
	private TrackingService trackingService;
	
	@CrossOrigin(origins = "*")
	@PostMapping("/obtenerTracking")
	public List<TrackingModel> obtenerTracking(@RequestBody TrackingRequest trackingRequest) {
		log.info("INGRESA A ===> obtenerTracking("+trackingRequest+")");
		return trackingService.getTracking(trackingRequest);
	}
}
