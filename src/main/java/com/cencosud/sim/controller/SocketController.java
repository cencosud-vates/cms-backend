package com.cencosud.sim.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import com.cencosud.sim.model.response.SocketResponse;


@Controller
public class SocketController {
	
	@MessageMapping("/saludo")
    @SendTo("/tema/barras")
    public SocketResponse greeting(SocketResponse socket) {
        return new SocketResponse( socket.getSaludo() + " Funcionando!");
    }
}
