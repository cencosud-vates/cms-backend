package com.cencosud.sim.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cencosud.sim.model.TipoTransaccionModel;
import com.cencosud.sim.service.TipoTransaccionService;

@RestController
@RequestMapping("/rest")
public class TipoTransaccionController {
	static final Logger log = Logger.getLogger(TipoTransaccionController.class);
	
	@Autowired
	private TipoTransaccionService tipoTransaccionService;
	
	@CrossOrigin(origins = "*")
	@GetMapping("/tipoTransaccion")
	public List<TipoTransaccionModel> obtenerTipoTransaccion(){
		log.info("INGRESA A ===> obtenerTipoTransaccion()");
		return tipoTransaccionService.obtenerTipoTransaccion();
	}

}
