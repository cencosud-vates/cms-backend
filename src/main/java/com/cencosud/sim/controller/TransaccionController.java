package com.cencosud.sim.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cencosud.sim.model.response.CargasPcbsResponse;
import com.cencosud.sim.service.TransaccionService;

@RestController
@RequestMapping("/rest")
public class TransaccionController {
	static final Logger log = Logger.getLogger(TransaccionController.class);
	
	@Autowired
	@Qualifier("TransaccionService")
	private TransaccionService transaccionService;
	
	/**
	 * Obtiene el listado de transacciones a procesar que se han cargado
	 * @return
	 */
	@CrossOrigin(origins = "*")
	@GetMapping("/obtenerTransacciones")
	public List<CargasPcbsResponse> obtenerTransacciones(){
		log.info("INGRESA A ===> obtenerTransacciones()");
		return transaccionService.obtenerTransacciones();
		
	}

}
