package com.cencosud.sim.dto;

import java.sql.Date;

public class Tracking {

	private Integer id;
	private String descripcion;
	private Date fecha;
	private String usuario;
	
	private Integer simOk;
	private Integer simNok;
	private Integer pendiente;
	private Integer digitalOk;
	private Integer digitalNok;
	private Integer manualOk;
	private Integer manualNok;
	private Integer fileShare;
	private Integer idOriginal;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Integer getSimOk() {
		return simOk;
	}
	public void setSimOk(Integer simOk) {
		this.simOk = simOk;
	}
	public Integer getSimNok() {
		return simNok;
	}
	public void setSimNok(Integer simNok) {
		this.simNok = simNok;
	}
	public Integer getPendiente() {
		return pendiente;
	}
	public void setPendiente(Integer pendiente) {
		this.pendiente = pendiente;
	}
	public Integer getDigitalOk() {
		return digitalOk;
	}
	public void setDigitalOk(Integer digitalOk) {
		this.digitalOk = digitalOk;
	}
	public Integer getDigitalNok() {
		return digitalNok;
	}
	public void setDigitalNok(Integer digitalNok) {
		this.digitalNok = digitalNok;
	}
	public Integer getManualOk() {
		return manualOk;
	}
	public void setManualOk(Integer manualOk) {
		this.manualOk = manualOk;
	}
	public Integer getManualNok() {
		return manualNok;
	}
	public void setManualNok(Integer manualNok) {
		this.manualNok = manualNok;
	}
	public Integer getFileShare() {
		return fileShare;
	}
	public void setFileShare(Integer fileShare) {
		this.fileShare = fileShare;
	}
	public Integer getIdOriginal() {
		return idOriginal;
	}
	public void setIdOriginal(Integer idOriginal) {
		this.idOriginal = idOriginal;
	}
}
