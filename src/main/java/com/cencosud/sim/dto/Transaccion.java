package com.cencosud.sim.dto;

import java.sql.Date;

public class Transaccion {

	private Integer id;
	private String descripcion;
	private Date fecha;
	private Integer pendiente;
	private String estado;
	private Accion accion;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Integer getPendiente() {
		return pendiente;
	}
	public void setPendiente(Integer pendiente) {
		this.pendiente = pendiente;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Accion getAccion() {
		return accion;
	}
	public void setAccion(Accion accion) {
		this.accion = accion;
	}
}
