package com.cencosud.sim.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.cencosud.sim.model.ItemPropuestaModel;
import com.cencosud.sim.model.request.PropuestaValorRequest;

@Mapper
public interface ItemPropuestaMapper {

	List<ItemPropuestaModel> obtenerItemPropuestas(@Param("pptaId") int pptaId);

	Long obtenerValorItem(@Param("itemPropuestaModel") ItemPropuestaModel itemPropuestaModel);

	int insertarItemsPropuesta(@Param("itemPropuestaModel") ItemPropuestaModel itemPropuestaModel);

	List<ItemPropuestaModel> obtenerItemPropuesta(@Param("pllaId") int pllaId,@Param("pptaId") int pptaId);

	void ejecutarQuery(@Param("query") String query);

	List<Long> obtenerItemPropuestasMas(@Param("itemId") int itemId,@Param("txId") int txId);

	int insertarItemsPropuestaMas(@Param("itemPropuestas") List<ItemPropuestaModel> itemPropuestas);

	List<Long> obtenerItems(@Param("txId") int txId);

	List<PropuestaValorRequest> cuerpoPropuestaValor(@Param("txId") int txId);

	String obtenerQuery(@Param("itemId") int itemId);

}
