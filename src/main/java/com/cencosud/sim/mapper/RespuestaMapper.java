package com.cencosud.sim.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.cencosud.sim.model.RespuestaModel;

@Mapper
public interface RespuestaMapper {

	void registrarRespuesta(@Param("list") List<RespuestaModel> list);

	List<RespuestaModel> consultaRespuesta(@Param("respuestas") List<RespuestaModel> respuestas);

	void registrarRespuestaEmail(@Param("list") List<RespuestaModel> list);

	List<RespuestaModel> obtenerRespuestasEmail(@Param("recId") int recId);


}
