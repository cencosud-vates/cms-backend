package com.cencosud.sim.model;

import java.io.Serializable;

public class OperacionModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long operId;
	private String operEstado;
	private String operFchEnvio;
	private Long txId;
	private Long pcbsId;
	private int errorId;
	public Long getOperId() {
		return operId;
	}
	public void setOperId(Long operId) {
		this.operId = operId;
	}
	public String getOperEstado() {
		return operEstado;
	}
	public void setOperEstado(String operEstado) {
		this.operEstado = operEstado;
	}
	public String getOperFchEnvio() {
		return operFchEnvio;
	}
	public void setOperFchEnvio(String operFchEnvio) {
		this.operFchEnvio = operFchEnvio;
	}
	public Long getTxId() {
		return txId;
	}
	public void setTxId(Long txId) {
		this.txId = txId;
	}
	public Long getPcbsId() {
		return pcbsId;
	}
	public void setPcbsId(Long pcbsId) {
		this.pcbsId = pcbsId;
	}
	public int getErrorId() {
		return errorId;
	}
	public void setErrorId(int errorId) {
		this.errorId = errorId;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OperacionModel [operId=");
		builder.append(operId);
		builder.append(", operEstado=");
		builder.append(operEstado);
		builder.append(", operFchEnvio=");
		builder.append(operFchEnvio);
		builder.append(", txId=");
		builder.append(txId);
		builder.append(", pcbsId=");
		builder.append(pcbsId);
		builder.append(", errorId=");
		builder.append(errorId);
		builder.append("]");
		return builder.toString();
	}
	
}
