package com.cencosud.sim.model;

import java.io.Serializable;

public class TipoTransaccionModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int tptxId;
	private String tptxCodigo;
	private String tptxDescripcion;
	public int getTptxId() {
		return tptxId;
	}
	public void setTptxId(int tptxId) {
		this.tptxId = tptxId;
	}
	public String getTptxCodigo() {
		return tptxCodigo;
	}
	public void setTptxCodigo(String tptxCodigo) {
		this.tptxCodigo = tptxCodigo;
	}
	public String getTptxDescripcion() {
		return tptxDescripcion;
	}
	public void setTptxDescripcion(String tptxDescripcion) {
		this.tptxDescripcion = tptxDescripcion;
	}
	@Override
	public String toString() {
		return "TipoTransaccionModel [tptxId=" + tptxId + ", tptxCodigo=" + tptxCodigo + ", tptxDescripcion="
				+ tptxDescripcion + "]";
	}
	
	

}
