package com.cencosud.sim.model;

import java.io.Serializable;
import java.sql.Date;

public class TrackingModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String tipoCarga;
	private Date fechaCarga;
	private Date fechaInicioCarga;
	private Date fechaTerminoCarga;
	private int origen;
	private int aProcesarSim;
	private int validado;
	private int validadoError;
	private int procesado;
	private int procasadoError;
	private int fileshare;
	private int despachoDigital;
	private int despachoTradicional;
	private int respDigEntregado;
	private int respDigNoEntregado;
	private int respManEntregado;
	private int respManNoEntregado;
	private int otroId;
	private boolean respDigRojo;
	private boolean idNoAsignable;
	private int enOtroId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTipoCarga() {
		return tipoCarga;
	}
	public void setTipoCarga(String tipoCarga) {
		this.tipoCarga = tipoCarga;
	}
	public Date getFechaCarga() {
		return fechaCarga;
	}
	public void setFechaCarga(Date fechaCarga) {
		this.fechaCarga = fechaCarga;
	}
	public Date getFechaInicioCarga() {
		return fechaInicioCarga;
	}
	public void setFechaInicioCarga(Date fechaInicioCarga) {
		this.fechaInicioCarga = fechaInicioCarga;
	}
	public Date getFechaTerminoCarga() {
		return fechaTerminoCarga;
	}
	public void setFechaTerminoCarga(Date fechaTerminoCarga) {
		this.fechaTerminoCarga = fechaTerminoCarga;
	}
	public int getOrigen() {
		return origen;
	}
	public void setOrigen(int origen) {
		this.origen = origen;
	}
	public int getaProcesarSim() {
		return aProcesarSim;
	}
	public void setaProcesarSim(int aProcesarSim) {
		this.aProcesarSim = aProcesarSim;
	}
	public int getValidado() {
		return validado;
	}
	public void setValidado(int validado) {
		this.validado = validado;
	}
	public int getValidadoError() {
		return validadoError;
	}
	public void setValidadoError(int validadoError) {
		this.validadoError = validadoError;
	}
	public int getProcesado() {
		return procesado;
	}
	public void setProcesado(int procesado) {
		this.procesado = procesado;
	}
	public int getProcasadoError() {
		return procasadoError;
	}
	public void setProcasadoError(int procasadoError) {
		this.procasadoError = procasadoError;
	}
	public int getFileshare() {
		return fileshare;
	}
	public void setFileshare(int fileshare) {
		this.fileshare = fileshare;
	}
	public int getDespachoDigital() {
		return despachoDigital;
	}
	public void setDespachoDigital(int despachoDigital) {
		this.despachoDigital = despachoDigital;
	}
	public int getDespachoTradicional() {
		return despachoTradicional;
	}
	public void setDespachoTradicional(int despachoTradicional) {
		this.despachoTradicional = despachoTradicional;
	}
	public int getRespDigEntregado() {
		return respDigEntregado;
	}
	public void setRespDigEntregado(int respDigEntregado) {
		this.respDigEntregado = respDigEntregado;
	}
	public int getRespDigNoEntregado() {
		return respDigNoEntregado;
	}
	public void setRespDigNoEntregado(int respDigNoEntregado) {
		this.respDigNoEntregado = respDigNoEntregado;
	}
	public int getRespManEntregado() {
		return respManEntregado;
	}
	public void setRespManEntregado(int respManEntregado) {
		this.respManEntregado = respManEntregado;
	}
	public int getRespManNoEntregado() {
		return respManNoEntregado;
	}
	public void setRespManNoEntregado(int respManNoEntregado) {
		this.respManNoEntregado = respManNoEntregado;
	}
	public int getOtroId() {
		return otroId;
	}
	public void setOtroId(int otroId) {
		this.otroId = otroId;
	}
	public boolean isRespDigRojo() {
		return respDigRojo;
	}
	public void setRespDigRojo(boolean respDigRojo) {
		this.respDigRojo = respDigRojo;
	}
	public boolean isIdNoAsignable() {
		return idNoAsignable;
	}
	public void setIdNoAsignable(boolean idNoAsignable) {
		this.idNoAsignable = idNoAsignable;
	}
	public int getEnOtroId() {
		return enOtroId;
	}
	public void setEnOtroId(int enOtroId) {
		this.enOtroId = enOtroId;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrackingModel [id=");
		builder.append(id);
		builder.append(", tipoCarga=");
		builder.append(tipoCarga);
		builder.append(", fechaCarga=");
		builder.append(fechaCarga);
		builder.append(", fechaInicioCarga=");
		builder.append(fechaInicioCarga);
		builder.append(", fechaTerminoCarga=");
		builder.append(fechaTerminoCarga);
		builder.append(", origen=");
		builder.append(origen);
		builder.append(", aProcesarSim=");
		builder.append(aProcesarSim);
		builder.append(", validado=");
		builder.append(validado);
		builder.append(", validadoError=");
		builder.append(validadoError);
		builder.append(", procesado=");
		builder.append(procesado);
		builder.append(", procasadoError=");
		builder.append(procasadoError);
		builder.append(", fileshare=");
		builder.append(fileshare);
		builder.append(", despachoDigital=");
		builder.append(despachoDigital);
		builder.append(", despachoTradicional=");
		builder.append(despachoTradicional);
		builder.append(", respDigEntregado=");
		builder.append(respDigEntregado);
		builder.append(", respDigNoEntregado=");
		builder.append(respDigNoEntregado);
		builder.append(", respManEntregado=");
		builder.append(respManEntregado);
		builder.append(", respManNoEntregado=");
		builder.append(respManNoEntregado);
		builder.append(", otroId=");
		builder.append(otroId);
		builder.append(", respDigRojo=");
		builder.append(respDigRojo);
		builder.append(", idNoAsignable=");
		builder.append(idNoAsignable);
		builder.append(", enOtroId=");
		builder.append(enOtroId);
		builder.append("]");
		return builder.toString();
	}
}
