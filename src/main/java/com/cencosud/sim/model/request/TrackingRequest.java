package com.cencosud.sim.model.request;

import java.io.Serializable;

public class TrackingRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int origen;
	private String fechaDesde;
	private String fechaHasta;
	private int correlativo;
	private String usuario;
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public int getOrigen() {
		return origen;
	}
	public void setOrigen(int origen) {
		this.origen = origen;
	}
	public String getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public String getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public int getCorrelativo() {
		return correlativo;
	}
	public void setCorrelativo(int correlativo) {
		this.correlativo = correlativo;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TrackingRequest [origen=");
		builder.append(origen);
		builder.append(", fechaDesde=");
		builder.append(fechaDesde);
		builder.append(", fechaHasta=");
		builder.append(fechaHasta);
		builder.append(", correlativo=");
		builder.append(correlativo);
		builder.append(", usuario=");
		builder.append(usuario);
		builder.append("]");
		return builder.toString();
	}
}
