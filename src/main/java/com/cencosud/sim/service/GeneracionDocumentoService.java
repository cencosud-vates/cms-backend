package com.cencosud.sim.service;

import com.cencosud.sim.model.request.CargaPcbsRequest;
import com.cencosud.sim.model.response.MessageResponse;

public interface GeneracionDocumentoService {

	MessageResponse procesarDocumentos(CargaPcbsRequest cargaPcbsRequest);

	MessageResponse validarCarga(CargaPcbsRequest cargaPcbsRequest);

	MessageResponse generarDocumentos(CargaPcbsRequest cargaPcbsRequest);

	MessageResponse reValidar(CargaPcbsRequest cargaPcbsRequest);

	MessageResponse distribuir(CargaPcbsRequest cargaPcbsRequest);

}
