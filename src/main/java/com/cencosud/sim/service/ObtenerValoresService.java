package com.cencosud.sim.service;

import java.util.List;

public interface ObtenerValoresService {

	String obtenerItem(int itemId);

	String obtenerValorItem(String sqlQuery, String valorBuscado);

	List<String> obtenerPlantillas();


}
