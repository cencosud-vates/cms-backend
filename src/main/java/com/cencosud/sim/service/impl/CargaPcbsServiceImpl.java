package com.cencosud.sim.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.cencosud.sim.constants.Constants;
import com.cencosud.sim.mapper.CargaPcbsMapper;
import com.cencosud.sim.model.CargaPcbsModel;
import com.cencosud.sim.model.RespuestaModel;
import com.cencosud.sim.model.TransaccionModel;
import com.cencosud.sim.model.request.CargaPcbsRequest;
import com.cencosud.sim.model.request.ExcelRequest;
import com.cencosud.sim.model.response.CargasPcbsResponse;
import com.cencosud.sim.service.CargaPcbsService;
import com.cencosud.sim.service.ItemPropuestaService;
import com.cencosud.sim.service.PlantillaService;
import com.cencosud.sim.service.TransaccionService;
import com.cencosud.sim.util.Util;

@Service("CargaPcbsService")
public class CargaPcbsServiceImpl implements CargaPcbsService{
	static final Logger log = Logger.getLogger(CargaPcbsServiceImpl.class);
	
	@Autowired
	@Qualifier("TransaccionService")
	private TransaccionService transaccionService;
	
	@Autowired
	@Qualifier("PlantillaService")
	private PlantillaService plantillaService;
	
	@Autowired
	@Qualifier("ItemPropuestaService")
	private ItemPropuestaService itemPropuestaService;
	
	@Autowired
	private CargaPcbsMapper cargaPcbsMapper;

	/**
	 * Realiza la carga de tabla carga_pcbs ejecutando procedimiento almacenado seleccionado en combo
	 * de origen. Para ello es necesario insertar 1 registro en la tabla transacción.
	 */
	@Override
	public CargasPcbsResponse procesarCargaPcbs(CargaPcbsRequest cargaPcbsRequest) {
		log.info("INGRESA A ===> procesarCargaPcbs("+cargaPcbsRequest+")");
		//TODO revisar
		CargaPcbsRequest cargaPcbsVerificada = verificarFechaRequest(cargaPcbsRequest);
	    
		CargasPcbsResponse cargasPcbsResponse = new CargasPcbsResponse();
		try {
			
			cargaPcbsVerificada.setEstado(Constants.PENDIENTE);
			TransaccionModel transaccionModel = transaccionService.registrarTransaccion(cargaPcbsVerificada);
			
			cargaPcbsVerificada.setTxId(transaccionModel.getTxId());
			//Realiza la carga masiva de la tabla carga_pcbs
			realizarCargaPcbs(cargaPcbsVerificada);
			
			transaccionModel.setTxIdPadre(transaccionModel.getTxId());
			//Guarda la transacción
			guardarTransaccion(transaccionModel);
			
			cargasPcbsResponse = transaccionService.obtenerTransaccion(transaccionModel.getTxId());
		} catch (Exception e) {
			log.error(e);
		}
		
		return cargasPcbsResponse;
	}
	


	private CargaPcbsRequest verificarFechaRequest(CargaPcbsRequest cargaPcbsRequest) {
		SimpleDateFormat diaMesAnio = new SimpleDateFormat(Constants.FECHA_FRONT);
	    SimpleDateFormat diaMesAnioGuion = new SimpleDateFormat(Constants.FECHA_BACK);
	    Date termino = new Date();
	    Date inicio = new Date();
		try {
			termino = diaMesAnio.parse(cargaPcbsRequest.getTxFchTermino());
			inicio = diaMesAnio.parse(cargaPcbsRequest.getTxFchInicio());
		} catch (ParseException e1) {
			log.error(e1);
		}
		cargaPcbsRequest.setTxFchTermino(diaMesAnioGuion.format(termino));
		cargaPcbsRequest.setTxFchInicio(diaMesAnioGuion.format(inicio));
	    
		return cargaPcbsRequest;
	}



	/**
	 * Realiza carga masiva en la tabla carga_pcbs ejecutando el procedimiento almacenado seleccionado
	 * @param procesarCargaMap
	 * @return
	 */
	private boolean realizarCargaPcbs(CargaPcbsRequest cargaPcbsRequest) {
		try {
			log.info("Inicia Carga de datos CARGA_PCBS para TX_ID:" + cargaPcbsRequest.getTptxId());
			Object respuesta = cargaPcbsMapper.realizarCargaPcbs(cargaPcbsRequest);
			if(respuesta instanceof String) {
				log.error(respuesta);
			}else {
				log.info("SE HA REALIZADO CARGA DE DATOS EN CARGA_PCBS");
				log.info(respuesta+" REGISTROS");
			}
		} catch (Exception e) {
			log.error(e);
		}
		
		return true;
	}

	
	/**
	 * Se actualiza el campo fecha de termino para la transacción recien ejecutada
	 * @param txId
	 * @return
	 */
	private boolean guardarTransaccion(TransaccionModel transaccionModel) {
		return transaccionService.guardarTransaccion(transaccionModel)>0;
	}



	@Override
	public List<CargaPcbsModel> obtenerCargaPcbs(ExcelRequest excelRequest) {
		return cargaPcbsMapper.obtenerCargaPcbs(excelRequest);
	}



	@Override
	public void registrarIdEmail(List<RespuestaModel> respuestas, int nuevoId) {
		log.info("INGRESA A ===> registrarIdEmail("+respuestas+","+nuevoId+")");
		List<RespuestaModel> respuestasFiltro = new ArrayList<>();
		
		for (int i = 0; i < respuestas.size(); i++) {
			if(Constants.EXCEL_MAIL_NOENVIADO.equalsIgnoreCase(respuestas.get(i).getRespEstado())) {
				respuestas.get(i).setTxIdOriginal(respuestas.get(i).getRespTxId());
				respuestas.get(i).setRespTxId(nuevoId);
				respuestasFiltro.add(respuestas.get(i));
				log.info(respuestas.get(i));
			}
		}
		
		Util util = new Util();
		List<List<RespuestaModel>> respuestasArray = util.cortarArray(respuestasFiltro, 1000);
		for (List<RespuestaModel> list : respuestasArray) {
			cargaPcbsMapper.registrarIdEmail(list);
		}
		
	}

	
	@Override
	public CargaPcbsModel obtenerPrimeraCargaPcbsXProp(int txId, int idPropuesta, int idPlan) {
		List<CargaPcbsModel> cargaPcbsModel = cargaPcbsMapper.obtenerCargaPcbsXProp(txId, idPropuesta);
		if(cargaPcbsModel != null && cargaPcbsModel.size() > 0){
			return cargaPcbsModel.get(0);
		} else {
			return null;
		}
	}


}
