package com.cencosud.sim.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cencosud.sim.mapper.EmpresaMapper;
import com.cencosud.sim.model.EmpresaModel;
import com.cencosud.sim.model.request.EmpresaRequest;
import com.cencosud.sim.service.EmpresaService;

@Service("EmpresaService")
public class EmpresaServiceImpl implements EmpresaService{
	static final Logger log = Logger.getLogger(EmpresaServiceImpl.class);
	@Autowired
	private EmpresaMapper empresaMapper;

	@Override
	public List<EmpresaModel> obtenerEmpresaRecepcion(EmpresaRequest empresaRequest) {
		List<EmpresaModel> empresas = null;
		try {
			empresas = empresaMapper.obtenerEmpresaRecepcion(empresaRequest);
		} catch (Exception e) {
			log.error(e);
		}
		return empresas;
	}

	@Override
	public EmpresaModel obtenerEmpresaXID(int empId) {
		EmpresaModel empresa = null;
		try {
			empresa = empresaMapper.obtenerEmpresaXID(empId);
		} catch (Exception e) {
			log.error(e);
		}
		return empresa;
	}
	
	
	


}
