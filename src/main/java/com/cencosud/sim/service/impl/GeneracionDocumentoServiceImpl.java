package com.cencosud.sim.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import com.cencosud.sim.constants.Constants;
import com.cencosud.sim.model.CargaPcbsModel;
import com.cencosud.sim.model.EmpresaModel;
import com.cencosud.sim.model.ErrorsModel;
import com.cencosud.sim.model.PlantillaModel;
import com.cencosud.sim.model.PlantillaPropuestaModel;
import com.cencosud.sim.model.TransaccionModel;
import com.cencosud.sim.model.request.CargaPcbsRequest;
import com.cencosud.sim.model.response.MessageResponse;
import com.cencosud.sim.model.response.SocketResponse;
import com.cencosud.sim.service.CargaPcbsService;
import com.cencosud.sim.service.EmpresaService;
import com.cencosud.sim.service.GeneracionDocumentoService;
import com.cencosud.sim.service.ItemPropuestaService;
import com.cencosud.sim.service.OperacionService;
import com.cencosud.sim.service.PlantillaService;
import com.cencosud.sim.service.TransaccionService;
import com.cencosud.sim.util.CrearPolizaPdf;
import com.cencosud.sim.util.ExcelUtil;
import com.cencosud.sim.util.Items;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;

import static java.lang.Math.toIntExact;

@Service("GeneracionDocumentoService")
public class GeneracionDocumentoServiceImpl extends TextWebSocketHandler implements GeneracionDocumentoService {
	static final Logger log = Logger.getLogger(GeneracionDocumentoServiceImpl.class);
	
	 WebSocketSession session;
	
	@Autowired
	@Qualifier("PlantillaService")
	private PlantillaService plantillaService;
	
	@Autowired
	@Qualifier("TransaccionService")
	private TransaccionService transaccionService;
	
	@Autowired
	@Qualifier("CargaPcbsService")
	private CargaPcbsService cargaPcbsService;
	
	@Autowired
	@Qualifier("ItemPropuestaService")
	private ItemPropuestaService itemPropuestaService;
	
	@Autowired
	@Qualifier("OperacionService")
	private OperacionService operacionService;
	
	@Autowired
	@Qualifier("ItemPropuestaService")
	private ItemPropuestaService itemPropuestaItem;
	
	@Autowired
	@Qualifier("EmpresaService")
	private EmpresaService empresaService;
	
	@Autowired
    private SimpMessagingTemplate template;
	
	/**
	 * Realiza validaciones de la carga
	 */
	@Override
	public MessageResponse validarCarga(CargaPcbsRequest cargaPcbsRequest) {
		
		MessageResponse mensaje;
		CargaPcbsRequest cargaPcbs;
		this.template.convertAndSend(Constants.SOCKET, new SocketResponse(cargaPcbsRequest.getTxId(),0,"Validaciones","Se están realizando validaciones",""));
		TransaccionModel ultimaTransaccion = transaccionService.obtenerUltimaTransaccion(cargaPcbsRequest.getTxId());
		boolean enEjecucion = false;
		int errores = 0;
		cargaPcbsRequest.setEstado(Constants.VALIDADO);
		if(ultimaTransaccion.getTxFchTermino()==null  
				|| (ultimaTransaccion.getTxFchTermino() != null && ultimaTransaccion.getEstId() == Constants.VALIDADO)
				) {
			enEjecucion = true;
			
		}else {
			//Registra transaccion
			cargaPcbs = obtenerTransaccionPadre(cargaPcbsRequest);
			TransaccionModel transaccion = transaccionService.registrarTransaccion(cargaPcbs);
			
			//Realiza validaciones
			errores = validarExistenciaArchivo(cargaPcbs);
			errores += validarComuna(cargaPcbs);
			
			//Guarda transaccion
			transaccion.setEstId(errores > Constants.CERO?Constants.VALIDADO_ERROR:Constants.VALIDADO);
			transaccionService.guardarTransaccion(transaccion);
		}
		
		if(enEjecucion) {
			cargaPcbsRequest.setEstado(Constants.VALIDADO);
		} else if(errores>Constants.CERO){
			cargaPcbsRequest.setEstado(Constants.SIN_PLANTILLA);
		}else {
			cargaPcbsRequest.setEstado(Constants.SIN_ERROR);
		}
		
		mensaje = transaccionService.obtenerMensaje(cargaPcbsRequest);
		mensaje.setError(enEjecucion);
		log.info(mensaje);
		
		return mensaje;
	}
	
	/**
	 * Obtiene el tipo de transaccion de la transaccion padre
	 * @param cargaPcbsRequest
	 * @return
	 */
	private CargaPcbsRequest obtenerTransaccionPadre(CargaPcbsRequest cargaPcbsRequest) {
		try {
			TransaccionModel transaccion = transaccionService.obtenerTransaccionPadre(cargaPcbsRequest.getTxId()); 
			log.info(transaccion);
			cargaPcbsRequest.setTptxId(transaccion.getTptxId());
		} catch (Exception e) {
			log.error(e);
		}
		return cargaPcbsRequest;
	}

	/**
	 * Valida la existencia de archivos de plantillas, de no existir se registra en la tabla operacion
	 * @param cargaPcbsRequest
	 */
	private int validarExistenciaArchivo(CargaPcbsRequest cargaPcbsRequest) {
		
	    int errores = Constants.CERO;
	    int contador = Constants.CERO;
	    List<PlantillaModel> plantillaList = plantillaService.obtenerPlantillas(cargaPcbsRequest.getTxId());
	    log.info("PASO POR validarExistenciaArchivo("+cargaPcbsRequest+") ");
	    int total = plantillaList.size() + 1;
	    for (PlantillaModel plantilla : plantillaList) {
			boolean existe = buscarArchivoServidor(plantilla.getPllaRuta());
			
			SocketResponse socket = new SocketResponse();
			socket.setId(cargaPcbsRequest.getTxId());
			socket.setMensaje(String.valueOf(plantilla.getPllaId()));
			socket.setDetalle("Se está validando plantilla "+plantilla.getPllaRuta());
			socket.setPorcentaje(++contador*100/total);
			log.info("VALIDACION "+total + " | "+ contador);
			this.template.convertAndSend(Constants.SOCKET, socket);
			if(existe)
				errores += registrarErrores(cargaPcbsRequest.getTxId(), plantilla.getPllaId(),2);
		}
		
		return errores;
	}
	
	private boolean buscarArchivoServidor(String nombreArchivo){
		String nombreArchivoLimpio = nombreArchivo.replaceAll("\\/", "");
		String path = Constants.SERVER + nombreArchivoLimpio;
		String username = Constants.USERNAMESERVER;
	    String password = Constants.PASSWORD;
		SmbFile file = null;
        try {
        	String url = path;
		    NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, username, password);
		    file = new SmbFile(url, auth);
		    log.info(file.getPath());
		    log.info(file.exists());
		    return !file.exists();
        } catch (Exception e) {
			log.error(e);
		} 
        
        return false;
	}

	/**
	 * Valida que la comuna en tabla carga_pcbs corresponda al del cliente
	 * @param cargaPcbsRequest
	 * @return
	 */
	private int validarComuna(CargaPcbsRequest cargaPcbsRequest) {
		log.info("PASO POR validarComuna("+cargaPcbsRequest+") ");
		int errores = 0;
		try {
			errores = operacionService.validarComuna(cargaPcbsRequest);
			
			this.template.convertAndSend(Constants.SOCKET, new SocketResponse(cargaPcbsRequest.getTxId(),100,"Comunas","Se están validando las comunas ",""));
		} catch (Exception e) {
			log.error(e);
		}
		return errores;
	}
	
	/**
	 * Obtiene y procesa la data de los items y los guarda en las tablas items_plantilla e items_propuesta,
	 * esta data se utilizara para llenar las plantillas
	 */
	@Override
	public MessageResponse procesarDocumentos(CargaPcbsRequest cargaPcbsRequest) {
		log.info("ENTRO A procesarDocumentos(CargaPcbsRequest cargaPcbsRequest)");
		MessageResponse mensaje = new MessageResponse();
		CargaPcbsRequest cargaPcbs;
		boolean enEjecucion = false;
		int errores = 0;
		try {
			this.template.convertAndSend(Constants.SOCKET, new SocketResponse(cargaPcbsRequest.getTxId(),0,"Procesando","Se están procesano propuestas",""));
			TransaccionModel ultimaTransaccion = transaccionService.obtenerUltimaTransaccion(cargaPcbsRequest.getTxId());
			
			if(ultimaTransaccion.getTxFchTermino()==null 
					|| (ultimaTransaccion.getTxFchTermino() != null && ultimaTransaccion.getEstId() == Constants.PROCESADO)
					|| (ultimaTransaccion.getTxFchTermino() != null && ultimaTransaccion.getEstId() == Constants.PROCESADO_ERROR)) {
				enEjecucion = true;
				cargaPcbsRequest.setEstado(4);
			}else {
				Long erroresObtenidos = operacionService.obtenerErrores(cargaPcbsRequest.getTxId());
				errores =  erroresObtenidos != null ? erroresObtenidos.intValue() : null;
				cargaPcbsRequest.setEstado(errores > Constants.CERO?Constants.PROCESADO_ERROR:Constants.PROCESADO);
				cargaPcbs = obtenerTransaccionPadre(cargaPcbsRequest);
				TransaccionModel transaccion = transaccionService.registrarTransaccion(cargaPcbs);
				
				//Obtiene datos de plantillas para llenar item_plantilla
				List<PlantillaModel> plantillaList = plantillaService.obtenerPlantillas(cargaPcbs.getTxId());
				log.info("plantillaService.obtenerPlantillas("+cargaPcbs.getTxId()+")");
				plantillaService.guardarDatosPlantillas(plantillaList);
				
				//Obtiene datos de items propuestas
				List<Long> items = itemPropuestaService.obtenerItems(cargaPcbs.getTxId());
				log.info("itemPropuestaService.obtenerItems("+cargaPcbs.getTxId()+")");
				
				int total = items.size();
				int contador = 0;
				
				for (Long item : items) {
					contador++;
					this.template.convertAndSend(Constants.SOCKET, 
							new SocketResponse(cargaPcbs.getTxId(),
									(contador*100/total),
												"Item "+item,
												"Se están validando items para propuesta",
												""));
					obtenerItemPropuestasMas(toIntExact(item),cargaPcbs.getTxId());
				}
				transaccionService.guardarTransaccion(transaccion);
			}
			log.info(errores);
			
			if(enEjecucion) {
				cargaPcbsRequest.setEstado(Constants.PROCESADO);
			} else if(errores>Constants.CERO){
				cargaPcbsRequest.setEstado(Constants.ERROR_PROCESO);
			}else {
				cargaPcbsRequest.setEstado(Constants.SIN_ERROR);
			}
			
			mensaje = transaccionService.obtenerMensaje(cargaPcbsRequest);
			mensaje.setError(enEjecucion);
			log.info(mensaje);
			
			return mensaje;
		} catch (Exception e) {
			log.error(e);
		}
		return null;
	}
	
	/**
	 * Obtiene data desde pcbs para llenar la tabla items_propuesta
	 * de forma masiva
	 * @param itemId
	 * @param txId 
	 */
	private void obtenerItemPropuestasMas(int itemId, int txId) {
		try {
			log.info("obtenerItemPropuestasMas("+itemId+")");
			String queryBase = itemPropuestaItem.obtenerQuery(itemId);
			log.info(queryBase);
			List<Long> itemPropuestaList =  itemPropuestaItem.obtenerItemPropuestasMas(itemId,txId);
			
			List<List<Long>> itemPropuestaListCorto = cortarArray(itemPropuestaList,1000);
			for (List<Long> list : itemPropuestaListCorto) {
				String comparadorCorto = armarComparador(list);
				String query = queryBase.replaceAll("@comparador", comparadorCorto);
				itemPropuestaItem.ejecutarQuery(query);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}
	
	
	private String armarComparador(List<Long> itemPropuestaList) {
		String comparador = "";
		//Arma query para traer data de items_propuestas
		for (int i = 0; i < itemPropuestaList.size(); i++) {
			if(i < itemPropuestaList.size() -1)
				comparador += itemPropuestaList.get(i)+",";
			else
				comparador += itemPropuestaList.get(i);
		}
		return comparador;
	}

	/**
	 * 
	 */
	@Override
	public MessageResponse generarDocumentos(CargaPcbsRequest cargaPcbsRequest) {
		log.info("PASO POR generarDocumentos("+cargaPcbsRequest+")");
		MessageResponse mensaje;
		this.template.convertAndSend(Constants.SOCKET, new SocketResponse(cargaPcbsRequest.getTxId(),0,"Generando Documento","Se están realizando generación de documentos",""));
		TransaccionModel ultimaTransaccion = transaccionService.obtenerUltimaTransaccion(cargaPcbsRequest.getTxId());
		boolean enEjecucion = false;
		if(ultimaTransaccion.getTxFchTermino()==null 
				|| (ultimaTransaccion.getTxFchTermino() != null && ultimaTransaccion.getEstId() == Constants.GENERADO)) {
			enEjecucion = true;
			cargaPcbsRequest.setEstado(Constants.GENERADO);
		}else {
			cargaPcbsRequest.setEstado(Constants.GENERADO);
			cargaPcbsRequest = obtenerTransaccionPadre(cargaPcbsRequest);
			TransaccionModel transaccion = transaccionService.registrarTransaccion(cargaPcbsRequest);
			
			//Genera polizas para distribuir
			generarPolizas(cargaPcbsRequest.getTxId());
			
			transaccionService.guardarTransaccion(transaccion);
			log.info("GENERO POLIZAS");
		}
		log.info(cargaPcbsRequest);
		mensaje = transaccionService.obtenerMensaje(cargaPcbsRequest);
		mensaje.setError(enEjecucion);
		log.info(mensaje);
		
		return mensaje;
	}
	
	private void obtenerArchivosBase(String dest) {
		String path = Constants.SERVER;
		String username = Constants.USERNAMESERVER;
	    String password = Constants.PASSWORD;
	    Set<String> ext = new HashSet<>();
	    ext.add("css");
	    ext.add("jpg");
	    ext.add("JPG");
	    ext.add("gif");
	    ext.add("GIF");
	    ext.add("js");
	    ext.add("JS");
	    try {
	    	copyExtraFile(path,username,password,ext,dest);
		} catch (Exception e) {
			log.error(e);
		}
	}

	/**
	 * Realiza la generacion de polizas segun datos de carga_pcbs filtrados por el numero de transaccion
	 * @param cargaPcbsModel
	 * @param transaccion
	 * @return
	 */
	private String generarPolizas(int txId) {
		log.info("PASO POR generarPolizas("+txId+")");
		HashSet<ErrorsModel> errores = new HashSet<>();
		Lock lock = new ReentrantLock(true);
		
		Map<String, List<Map<String , Object>>> modeloResumen = new ConcurrentHashMap<>();
		
		try {
			final AtomicInteger propCount = new AtomicInteger(0);
			String rutaRaiz = generarRutaRaiz(txId);
			Set<String> directoriosSalida = new CopyOnWriteArraySet<String>();
			
			List<PlantillaPropuestaModel> plantillaPropuestaList = plantillaService.buscarPlantilla(txId);
			
			//Generamos Mapa que agrupa plantillas segun numero propuesta
			Map<Integer, SortedSet<PlantillaPropuestaModel>> lstPropuestasGroup = 
					plantillaPropuestaList.stream()
										  .collect(Collectors.groupingBy(PlantillaPropuestaModel::getPptaId, 
												   Collectors.toCollection(TreeSet::new)));
			
			log.info("SE OBTIENEN "+ lstPropuestasGroup.size()+" PROPUESTAS PARA GENERAR POLIZAS");
			
			int total = lstPropuestasGroup.size() + 1; //Actualiza barra estado
			
			lstPropuestasGroup.entrySet()
							  .parallelStream()
							  .forEach((entry) -> {
								  
				Integer propuesta = entry.getKey();
				SortedSet<PlantillaPropuestaModel> plantillas = entry.getValue();
				log.info("Comienza logica para creacion de documento, propuesta: " + propuesta);
				log.info("Propuesta " + propCount.incrementAndGet() +" de "+ plantillaPropuestaList.size());
				
				String directorioSalida = generarRutaEspecifica(rutaRaiz, txId, propuesta, plantillas.first().getPlanId());
				lock.lock();
				try {
					if(!directoriosSalida.contains(directorioSalida)){
						directoriosSalida.add(directorioSalida);
						this.template.convertAndSend("/tema/barras", 
								new SocketResponse(txId,
										(propCount.get() + 1)*100/total,
										"",
										"Comienza carga de archivos base para empresa: " + obtenerEmpresaDespacho(txId, propuesta, plantillas.first().getPlanId()),
										""));
							obtenerArchivosBase(directorioSalida);
					}
				} finally {
					lock.unlock();
				}
				
				plantillas.forEach(plantilla -> {
					plantilla.setModeloDatos(itemPropuestaService.obtenerItemPropuesta(plantilla.getPllaId(),plantilla.getPptaId()));
					this.template.convertAndSend(Constants.SOCKET, 
							new SocketResponse(txId,
									(propCount.get() + 1)*100/total,
									plantilla.getPllaRuta(),
												"Se está generando documento para la propuesta "+plantilla.getPptaId(),
												""));
				});
				
				
				log.info("Comienza generacion PDF propuesta: " + propuesta);
				String archivoCreado = new CrearPolizaPdf().generacionPdf(propuesta, plantillas, directorioSalida);
				
				log.info("Generacion realizada exitosamente: " + archivoCreado);
				log.info("Se agrega propuesta a resumen");
				List<Map<String , Object>> lstDataResumen = new LinkedList<Map<String,Object>>();
				lstDataResumen.add(generaValueResumen(propuesta, archivoCreado, plantillas));
						 
				List<Map<String , Object>> lstPropResumen = modeloResumen.putIfAbsent(directorioSalida, lstDataResumen);
				
				if(lstPropResumen != null){
					lstPropResumen.addAll(lstDataResumen);
				}
				
				if(archivoCreado == null) {
					ErrorsModel error = new ErrorsModel();
					error.setTxId(txId);
					error.setPllaId(plantillas.first().getPllaId());
					error.setId(Constants.VALIDADO_ERROR);
					errores.add(error);
				}
			});
			
			//BORRAR EN SUB CARPETAS

			directoriosSalida.forEach(directorio -> {
				borrarArchivosBase(directorio);
			});
			
			//Genera Excel resumen
			generaExcelResumen(modeloResumen);
			
		} catch (Exception e) {
			log.error(e);
			log.error(ExceptionUtils.getStackTrace(e));
		}
		
		for (ErrorsModel error : errores) {
			registrarErrores(error.getTxId(), error.getPllaId(),error.getId());
		}
		return "";
	}
	
	private Map<String , Object> generaValueResumen(Integer propuesta, 
			                                        String archivoCreado, 
			                                        SortedSet<PlantillaPropuestaModel> plantillas){
		
		Map<String, Object> modeloDatosResumen = new HashMap<String, Object>();
		modeloDatosResumen.put(Items.NUM_PROPUESTA.getIdItem(), propuesta);
		modeloDatosResumen.put(Items.NOMBRE_ARCHIVO.getIdItem(), archivoCreado);
		plantillas.forEach(plantilla -> {
			modeloDatosResumen.put(Items.PLAN.getIdItem(), plantilla.getPlanId());
			modeloDatosResumen.putAll(plantilla.getModeloDatos());
		});
		return modeloDatosResumen;
	}
	
	private void generaExcelResumen(Map<String, List<Map<String , Object>>> modeloResumen) throws IOException{
		ExcelUtil.creaExcelResumen(modeloResumen);
	}
	
	/**
	 * Registra errores en la tabla operacion
	 * @param txId
	 * @param pllaId
	 * @param errorId
	 */
	private int registrarErrores(int txId, int pllaId, int errorId) {
		Map<String, Integer> error = new HashMap<>();
		error.put("txId", txId);
		error.put("pllaId", pllaId);
		error.put("errorId", errorId);
		int respuesta = operacionService.registrarErrores(error);
		log.info("Se han registrado errores : "+respuesta);
		return respuesta;
	}

	/**
	 * Genera estructura de carpetas de salida
	 * @param txId
	 * @return
	 */
	private String generarRutaRaiz(int txId) {
		TransaccionModel transaccion = transaccionService.obtenerTransaccionPadre(txId);
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = formatter.format(transaccion.getTxFchInicio());
		String directorioSalida = Constants.RUTA_ESCRITURA_ARCHIVOS
				                 +Constants.BARRA_DIAGONAL
				                 +fecha+"_"
				                 +transaccion.getTxId()
				                 +Constants.BARRA_DIAGONAL;
		File directorio = new File(directorioSalida);
		if(!directorio.exists())
			directorio.mkdirs();
		return directorioSalida;
	}
	
	private String generarRutaEspecifica(String directorioRaiz, int txId, int idPropuesta, int idPlan) {

		CargaPcbsModel cargaModel = cargaPcbsService.obtenerPrimeraCargaPcbsXProp(txId, idPropuesta, idPlan);
		
		int empIdDespacho = 0;
		try {
			empIdDespacho = Integer.parseInt(cargaModel.getEmpIdDespacho());
		} catch (Exception e) {
			log.error("No se pude determinar el ID de Empresa de despacho" , e);
		}
		
		EmpresaModel empresaModel = empresaService.obtenerEmpresaXID(empIdDespacho);		
		String directorioSalida = directorioRaiz
				                 +empresaModel.getEmpRazonSocial().trim()
				                 +Constants.BARRA_DIAGONAL;
		File directorio = new File(directorioSalida);
		if(!directorio.exists())
			directorio.mkdirs();
		return directorioSalida;
	}
	
	private String obtenerEmpresaDespacho(int txId, int idPropuesta, int idPlan){
		CargaPcbsModel cargaModel = cargaPcbsService.obtenerPrimeraCargaPcbsXProp(txId, idPropuesta, idPlan);
		int empIdDespacho = 0;
		try {
			empIdDespacho = Integer.parseInt(cargaModel.getEmpIdDespacho());
		} catch (Exception e) {
			log.error("No se pude determinar el ID de Empresa de despacho" , e);
		}
		
		EmpresaModel empresaModel = empresaService.obtenerEmpresaXID(empIdDespacho);
		return empresaModel.getEmpRazonSocial().trim();
	}

	@Override
	public MessageResponse reValidar(CargaPcbsRequest cargaPcbsRequest) {
		try {
			this.template.convertAndSend(Constants.SOCKET, new SocketResponse(cargaPcbsRequest.getTxId(),0,"Re Validación","Se está realizando re validación",""));
			operacionService.quitarErrores(cargaPcbsRequest.getTxId());
			return validarCarga(cargaPcbsRequest);
		} catch (Exception e) {
			log.error(e);
		}
		return null;
	}

	@Override
	public MessageResponse distribuir(CargaPcbsRequest cargaPcbsRequest) {
		cargaPcbsRequest.setEstado(Constants.DISTRIBUIDO);
		this.template.convertAndSend(Constants.SOCKET, new SocketResponse(cargaPcbsRequest.getTxId(),0,"Distribución","Se está realizando distribución",""));
		TransaccionModel ultimaTransaccion = transaccionService.obtenerUltimaTransaccion(cargaPcbsRequest.getTxId());
		boolean enEjecucion = false;
		if(ultimaTransaccion.getTxFchTermino()==null || (ultimaTransaccion.getTxFchTermino() != null && ultimaTransaccion.getEstId() == Constants.DISTRIBUIDO)) {
			enEjecucion = true;
		}else {
			cargaPcbsRequest = obtenerTransaccionPadre(cargaPcbsRequest);
			TransaccionModel transaccion = transaccionService.registrarTransaccion(cargaPcbsRequest);
			copyFiles(cargaPcbsRequest.getTxId());
			transaccionService.guardarTransaccion(transaccion);
			cargaPcbsRequest.setEstado(11);
		}
		
		MessageResponse mensaje = transaccionService.obtenerMensaje(cargaPcbsRequest);
		mensaje.setError(enEjecucion);
		log.info(mensaje);
		return mensaje;
	}
	
    /**
     * Copia archivos base como imagenes, estilos y otros a carpeta de salida para la generacion de polizas
     * @param path
     * @param username
     * @param password
     * @param ext
     * @param dest
     * @throws SmbException
     * @throws UnknownHostException
     * @throws IOException
     */
    private void copyExtraFile(String path, String username, String password, Set<String> ext, String dest) throws SmbException, UnknownHostException, IOException {
    	SmbFile dir = null;
        OutputStream os = null;
        try {
        	byte[] buffer = new byte[1024];
        	String url = path;
		    NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, username, password);
		    dir = new SmbFile(url, auth);
		    SmbFile[] files = dir.listFiles();
		    
		    for (SmbFile file : files) {
				if (!file.isDirectory() && ext.contains(getFileExtension(file.getName())) )  {
					log.info("========================================================");
					log.info(file.getCanonicalPath());
					 os = new FileOutputStream(new File(dest+file.getName()));
					    try (SmbFileInputStream in = new SmbFileInputStream(file)) {
					    	int length;
				            while ((length = in.read(buffer)) > 0) {
				                os.write(buffer, 0, length);
				            }
					    }catch (Exception e) {
							log.error(e);
						}
				} 
			}
        } catch (Exception e) {
			log.error(e);
		} 
        
        finally {
            os.close();
        }
    }
	
    /**
     * Obtiene la extension de un archivo
     * @param filename
     * @return
     */
	private String getFileExtension(String filename) {
		File file = new File(filename);
	    String name = file.getName();
	    try {
	        return name.substring(name.lastIndexOf(".") + 1);
	    } catch (Exception e) {
	        return "";
	    }
	}
	
	/**
	 * Borra archivos base para la generacion de archivos
	 * @param directorioSalida
	 */
	private void borrarArchivosBase(String directorioSalida) {
		try {
			File dir = new File(directorioSalida);
			File[] files = dir.listFiles();
			for (File file : files) {
				if( !getFileExtension(file.getName()).equals("pdf") ) {
					log.info("Archivo temporal "+file.getName()+" fue borrado");
					file.delete();
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
	}
	
	public void copyFiles(int txId) {
		String fileNameIn = generarRutaRaiz(txId);
		
		final File folder = new File(fileNameIn);
		log.info(folder);
		try {
			File ruta = new File(folder.getParent());
			File carpeta = new File(folder.getName());
			copyDirectory(ruta, carpeta,folder,txId);
		} catch (Exception e) {
			log.error(e);
		}
		listFilesForFolder(folder, folder,txId);
	}
	
	private void listFilesForFolder(final File folder, File folderBase, int txId) {
		int total = folder.listFiles().length;
		int contador = 0;
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	        	try {
		            listFilesForFolder(fileEntry,folderBase,txId);
				} catch (Exception e) {
					log.error(e);
				}
	        	
	        } else {
	        	log.info(fileEntry);
	        	File targetLocation = new File(fileEntry.getName());
	        	contador++;
		    	try {
		    		copyDirectory(fileEntry , targetLocation,folderBase,txId);
		    		this.template.convertAndSend(Constants.SOCKET, 
							new SocketResponse(txId,
									(contador*100/total),
									fileEntry.getName(),
									"Se está copiando archivo a fileshare",
									""));
				} catch (Exception e) {
					log.info(e);
				}
	        }
	    }
	}
	
	private void copyDirectory(File sourceLocation , File targetLocation, File folder, int txId)
		    throws IOException {
				
				String path = Constants.RUTA_FILE_SHARE+targetLocation;
				String username = Constants.USERNAMESERVER;
			    String password = Constants.PASSWORD;
			    
			    NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, username, password);

		        if (new File(sourceLocation.getPath()+Constants.BARRA_DIAGONAL+targetLocation.getName()).isDirectory()) {
		        	 SmbFile sFile = new SmbFile(path, auth);
		            if (!sFile.exists()) {
		            	sFile.mkdir();
		            }
		        } else {
		        	if("pdf".equalsIgnoreCase(getFileExtension(targetLocation.getName()))) {
		        		path = Constants.RUTA_FILE_SHARE+folder.getName()+Constants.BARRA_DIAGONAL+targetLocation;
			        	SmbFile sFile = new SmbFile(path, auth);
					    SmbFileOutputStream sfos = new SmbFileOutputStream(sFile);
					    InputStream in = new FileInputStream(new File(sourceLocation.getPath()));

			            // Copy the bits from instream to outstream
			            byte[] buf = new byte[1024];
			            int len;
			            while ((len = in.read(buf)) > 0) {
			            	sfos.write(buf, 0, len);
			            }
			            in.close();
			            sfos.close();
			            registrarArchivo(sFile,txId);
		        	}
		        }
		    }

	private void registrarArchivo(SmbFile sFile, int txId) {
		String nombre = sFile.getName();
		String nombreSinExt = nombre.split("\\.")[0];
		String[] nombreDescompuesto = nombreSinExt.split("-");
        int propuesta = Integer.parseInt(nombreDescompuesto[nombreDescompuesto.length -1]);
        
        String ruta = sFile.getPath().split("smb:")[1];
        String nombreArchivo = sFile.getName();
        transaccionService.guardarRutaArchivo(propuesta,nombreArchivo,ruta,txId);
        log.info("propuesta : "+propuesta+" nombreArchivo:"+nombreArchivo+" ruta:"+ruta+" txId:"+txId);
        log.info(sFile.getPath());
		
	}
	
	/**
	 * corta una lista para los casos en que es dificil de procesar
	 * @param list
	 * @param L
	 * @return
	 */
	private <T> List<List<T>> cortarArray(List<T> list, final int l) {
	    List<List<T>> parts = new ArrayList<>();
	    final int cantidad = list.size();
	    for (int i = 0; i < cantidad; i += l) {
	        parts.add(new ArrayList<T>(
	            list.subList(i, Math.min(cantidad, i + l)))
	        );
	    }
	    return parts;
	}
	
}
