package com.cencosud.sim.service.impl;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cencosud.sim.constants.Constants;
import com.cencosud.sim.mapper.TrackingMapper;
import com.cencosud.sim.model.TrackingModel;
import com.cencosud.sim.model.request.TrackingRequest;
import com.cencosud.sim.service.TrackingService;
import com.cencosud.sim.util.Util;

@Service("TrackingService")
public class TrackingServiceImpl implements TrackingService{
	static final Logger log = Logger.getLogger(TrackingServiceImpl.class);
	
	@Autowired
	private TrackingMapper trackingMapper;

	@Override
	public List<TrackingModel> getTracking(TrackingRequest trackingRequest) {
		List<TrackingModel> trackingList = null;
		try {
			Util util = new Util();
			if(trackingRequest.getFechaDesde() != null){
				trackingRequest.setFechaDesde(util.convertirFecha(trackingRequest.getFechaDesde())+Constants.HORA_INICIO);
				trackingRequest.setFechaHasta(util.convertirFecha(trackingRequest.getFechaHasta())+Constants.HORA_FIN);
			}
			trackingList = trackingMapper.getTracking(trackingRequest);
			for (int i = 0; i < trackingList.size(); i++) {
				int diferencia = trackingList.get(i).getDespachoDigital() -
				trackingList.get(i).getRespDigEntregado() - 
				trackingList.get(i).getRespDigNoEntregado();
				log.info(diferencia);
				if(diferencia>0) {
					log.info(trackingList.get(i).getId()+" TIENE DIFERENCIAS EN DESPACHO DIGITAL");
					trackingList.get(i).setRespDigRojo(true);
				}
				if(trackingList.get(i).getaProcesarSim() == 0) {
					trackingList.get(i).setIdNoAsignable(true);
				}
				if(trackingList.get(i).getFechaInicioCarga() 
						== null && trackingList.get(i).getFechaTerminoCarga() == null) {
					trackingList.get(i).setTipoCarga("Respuesta Digital "+trackingList.get(i).getTipoCarga());
					trackingList.get(i).setFechaInicioCarga(trackingList.get(i).getFechaCarga());
					trackingList.get(i).setFechaTerminoCarga(trackingList.get(i).getFechaCarga());
				}
				if(trackingList.get(i).getRespDigNoEntregado()>0) {
					int  enOtroId = trackingMapper.obtenerIdOrigen(trackingList.get(i).getId());
					trackingList.get(i).setEnOtroId(enOtroId);
				}
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return trackingList;
	}
}
