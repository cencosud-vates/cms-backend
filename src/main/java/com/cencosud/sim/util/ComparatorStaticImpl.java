package com.cencosud.sim.util;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class ComparatorStaticImpl {
	
	public enum TipoDoc {
		CERTIFICADOS(1, 1001),
		CARTA_BIENVENIDAS(2, 1),
		POLIZAS_GENERALE(3, 2),
		CONDICIONES_PARTICULARES(4, 3),
		PROCEDIMIENTO_CASO_SINIESTROS(5, 1002),
		ANULA_POLIZA(6, 1003),
		FICTICIO(7, 1004),
		CARTA_RECHAZO(8, 1005),
		CARTA_NN(9, 1006),
		ASISTENCIA_MULTI_ASIST(10, 1007),
		ASISTENCIA_GEA(11, 1008),
		ASISTENCIA_MEGASALUD(12, 1009),
		ASISTENCIA_SALCOBRAND(13, 1010),
		ASISTENCIA_CLINICA_SANTA_MARIA(14, 1011),
		ASISTENCIA_CRUZ_VERDE(15, 1012),
		ASISTENCIA_AXA(16, 1013),
		CARTA_RECHAZOS_PRODUCCION(17, 1014),
		CARTA_AVISO(18, 1015),
		CARTA_CORTE_EDAD(19, 1016),
		CARTA_WORKFLOW(20, 1017),
		CARTA_INSPECCION(21, 1018),
		VENTA_INSPECCION(22, 1019);
		
		private int idDoc;
		private int orden;
		
		private static final Map<Integer,TipoDoc> map;
	    static {
	        map = new HashMap<Integer,TipoDoc>();
	        for (TipoDoc v : TipoDoc.values()) {
	            map.put(v.idDoc, v);
	        }
	    }
		
		private TipoDoc (int idDoc, int orden){
			this.idDoc = idDoc;
			this.orden = orden;
		}
		
		public int getOrden(){
			return orden;
		}
		
		public static int getOrdenByID(int i){
			TipoDoc doc = map.get(i);
			if(doc == null){
				return 999999;
			}else {
				return doc.getOrden();
			}
		}
		
		public static TipoDoc findByKey(int i) {
	        return map.get(i);
	    }
		
	}
  
	

	private ComparatorStaticImpl() {
		super();
	}
	
	public static Comparator<String> nullSafeStringComparator = Comparator.nullsFirst(String::compareToIgnoreCase);
	
	public static Comparator<Integer> nullSafeIntComparator = Comparator
			.nullsFirst(Comparator.<Integer>naturalOrder());
	
	public static Comparator<Integer> docIdComparator = Comparator.nullsLast(new Comparator<Integer>() {
	    public int compare(Integer docId1, Integer docId2) {
	      return ((Integer)TipoDoc.getOrdenByID(docId1)).compareTo(TipoDoc.getOrdenByID(docId2));
	    }
	  });

}
