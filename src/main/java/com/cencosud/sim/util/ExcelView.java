package com.cencosud.sim.util;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

public class ExcelView extends AbstractXlsView{

/**
 * Realiza la carga de datos, tanto en cabecera como en cuerpo, de un archivo excel
 */
@Override
protected void buildExcelDocument(Map<String, Object> model,
                                  Workbook workbook,
                                  HttpServletRequest request,
                                  HttpServletResponse response) throws Exception {
    

    @SuppressWarnings("unchecked")
    Map<Integer, String> encabezado = (Map<Integer, String>) model.get("encabezado");
    
    @SuppressWarnings("unchecked")
	List<Map<Integer, Object>> cuerpo = (List<Map<Integer, Object>>) model.get("cuerpo");
    
    String nombre = "excel";
    
    
    
    // Cambia el nombre del archivo
    response.setHeader("Content-Disposition", "attachment; filename=\""+nombre+".xls\"");

    // Crea una hoja para archivos .xls
    Sheet sheet = workbook.createSheet("Detalle");
    

    // agrega estilos para aplicar a las celdas
    CellStyle style = workbook.createCellStyle();
    Font font = workbook.createFont();
    font.setFontName("Arial");
    style.setFillForegroundColor(HSSFColor.BLUE.index);
    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    font.setBold(true);
    font.setColor(HSSFColor.WHITE.index);
    style.setFont(font);


    // Crea fila de encabezado
    Row header = sheet.createRow(0);
    for (Map.Entry<Integer, String> entry : encabezado.entrySet()){
        header.createCell(entry.getKey()).setCellValue(entry.getValue());
        header.getCell(entry.getKey()).setCellStyle(style);
    }

    int rowCount = 1;
    //Carga data del cuerpo de la hoja
    for (Map<Integer, Object> map : cuerpo) {
    	Row cargaRow =  sheet.createRow(rowCount++);
        for (Map.Entry<Integer, Object> entry : map.entrySet()){
        	cargaRow.createCell(entry.getKey()).setCellValue(entry.getValue()==null?"":entry.getValue().toString());
        }
	}
    
    for (Map.Entry<Integer, String> entry : encabezado.entrySet()){
    	sheet.autoSizeColumn(entry.getKey());
    }
}

}