package com.cencosud.sim.util;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.cencosud.sim.constants.Constants;
import com.cencosud.sim.model.response.ResEmpresaResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class LeerArchivosExcel {
	static final Logger log = Logger.getLogger(LeerArchivosExcel.class);

	/**
	 * Lee un archivo y retorna su contenido, separado por encabezado y data 
	 * del contenido del archivo
	 * @param file
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public Map<String,Object> leerExcel(File file, String extension) {
		Map<String,Object> respuesta;
		Map<Integer,String> clave = new HashMap<>();
		List<Map<String,Object>> lectura = new ArrayList<>();
        try (
        		FileInputStream excelFile = new FileInputStream(file);
        		Workbook workbook = (Constants.EXTENSION_EXCEL_XLSX.equalsIgnoreCase(extension))? 
        				new XSSFWorkbook(excelFile):
        				new HSSFWorkbook(excelFile);
                
        	) {
            
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();
            
            
            
            while (iterator.hasNext()) {
                Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();
                Map<String,Object> objeto = new HashMap<>();
                while (cellIterator.hasNext()) {
                    Cell currentCell = cellIterator.next();
                    if(currentCell.getRowIndex()>0) {
                    	switch(currentCell.getCellTypeEnum()){
                        case STRING:
                        	objeto.put(clave.get(currentCell.getColumnIndex()), currentCell.getStringCellValue().trim());
                            break;
                        case NUMERIC:
                        	
                        	objeto.put(clave.get(currentCell.getColumnIndex()), tipoCelda(currentCell));
                            break;
                        default:
                            break;
                        }
                    }else {
                    	clave.put(currentCell.getColumnIndex(), currentCell.getStringCellValue().trim());
                    }
                }
                if(objeto.size()>0) {
                	lectura.add(objeto);
                	log.info(objeto);
                }
            }
            workbook.close();
            excelFile.close();
            
        } catch (IOException e) {
        	log.error(e);
		}
        respuesta = new HashMap<>();
        respuesta.put("lectura", lectura);
        respuesta.put("clave", clave);
        return respuesta;
    }
	
	/**
	 * Las celdas numericas pueden ser de tipo Date.
	 * Identifica el tipo de dato para las celdas numericas.
	 * @param currentCell
	 * @param celda 
	 * @return
	 */
	private Object tipoCelda(Cell currentCell) {
		Object respuesta = null;
    	switch (currentCell.getCellStyle().getDataFormat()) {
    	case 14:
		case 58:
			DateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");
			respuesta = fecha.format(currentCell.getDateCellValue());
			break;
		case 21:
		case 35:
			DateFormat hora = new SimpleDateFormat("HH:mm:ss");
			respuesta = hora.format(currentCell.getDateCellValue());
			break;
		default:
			respuesta = (int)currentCell.getNumericCellValue();
			break;
		}
		return respuesta;
	}

	/**
	 * Valida que el formato del archivo, o estructura de columnas, corresponde a la respuesta 
	 * de una empresa de correo tradicional o electronico.<br>
	 * La estructura comparada corresponde a la entregada por cencosud, no se considera otra estructura,
	 * pero se deja la opcion de cambiar en el archivo de configuracion "global.properties"
	 * @param encabezado
	 * @param empresa
	 * @param lectura
	 * @return
	 */
	public ResEmpresaResponse validarFormatoArchivo(String encabezado, int empresa, List<Map<String, Object>> lectura) {
		boolean valido = false;
		ResEmpresaResponse respuesta = new ResEmpresaResponse();
		if(Constants.EXCEL_FORMATO_CORREO_ELECTRONICO.equalsIgnoreCase(encabezado) && empresa==Constants.EMPRESA_CORREO_ELECTRONICO) {
			log.info("FORMATO DE ARCHIVO CORRESPONDE A CORREO ELECTRONICO");
			valido = true;
		} else if(Constants.EXCEL_FORMATO_CORREO_TRADICIONAL.equals(encabezado) && empresa!=Constants.EMPRESA_CORREO_ELECTRONICO) {
			log.info("FORMATO DE ARCHIVO CORRESPONDE A CORREO TRADICIONAL");
			valido = true;
		}else {
			respuesta.setError(true);
			respuesta.setDescripcion("El archivo cargado no corresponde a la empresa seleccionada");
			log.info("EL FORMATO DE ARCHIVO NO CORRESPONDE NI PARA CORREO ELECTRONICO NI PARA CORREO TRADICIONAL,\nREVISAR FORMATO, EL ENCABEZADO DEBE COINCIDIR");
		}
		if(valido) {
			respuesta = diferenciarEmpresa(lectura,empresa);
		}
		
		return respuesta;
	}
	
	/**
	 * Realiza diferenciacion entre las empresas para realizar el flujo correspondiente
	 * @param lectura
	 * @param empresa
	 * @return
	 */
	private ResEmpresaResponse diferenciarEmpresa(List<Map<String, Object>> lectura, int empresa) {
		ResEmpresaResponse respuesta = null;
		log.info("EMPRESA SELECCIONADA : "+empresa);
		if(Constants.EMPRESA_CORREO_ELECTRONICO == empresa) {
			log.info("ARCHIVO PARA CORREO ELECTRONICO");
			respuesta = contarRespuestaEmail(lectura);
		}else {
			log.info("ARCHIVO PARA CORREO TRADICIONAL");
			respuesta = contrarRespuestaTradicional(lectura);
		}
		return respuesta;
	}
	
	private ResEmpresaResponse contrarRespuestaTradicional(List<Map<String, Object>> lectura) {
		int entregado = 0;
		int noEntregado = 0;
		
		for (Map<String, Object> map : lectura) {
        	if(map.get(Constants.EXCEL_COLUMNA_ESTADO) != null &&
        		!map.get(Constants.EXCEL_COLUMNA_ESTADO).toString().trim()
        		.equalsIgnoreCase(Constants.EXCEL_TRADICIONAL_ENTREGADO)) {
        		noEntregado++;
        	}else if(map.get(Constants.EXCEL_COLUMNA_ESTADO) != null &&
            		map.get(Constants.EXCEL_COLUMNA_ESTADO).toString().trim()
            		.equalsIgnoreCase(Constants.EXCEL_TRADICIONAL_ENTREGADO)
        			) {
        		entregado++;
        	}
		}
		
		ResEmpresaResponse respuesta = new ResEmpresaResponse();
		respuesta.setLeido(lectura.size());
        respuesta.setEntregado(entregado);
        respuesta.setNoEntregado(noEntregado);
        respuesta.setError(false);
		return respuesta;
	}

	/**
	 * Cuenta registros para el tipo de respuesta "Correo Electronico"
	 * @param lectura
	 * @return
	 */
	private ResEmpresaResponse contarRespuestaEmail(List<Map<String, Object>> lectura) {
		int entregado = 0;
		int noEntregado = 0;
		
		for (Map<String, Object> map : lectura) {
			
        	if(map.get(Constants.EXCEL_COLUMNA_ESTADO) != null &&
        		map.get(Constants.EXCEL_COLUMNA_ESTADO).toString().trim()
        		.equalsIgnoreCase(Constants.EXCEL_MAIL_NOENVIADO)) {
        		noEntregado++;
        	}else if(map.get(Constants.EXCEL_COLUMNA_ESTADO) != null &&
            		map.get(Constants.EXCEL_COLUMNA_ESTADO).toString().trim()
            		.equalsIgnoreCase(Constants.EXCEL_MAIL_ENVIADO)
        			) {
        		entregado++;
        	}
		}
		
		ResEmpresaResponse respuesta = new ResEmpresaResponse();
		respuesta.setLeido(lectura.size());
        respuesta.setEntregado(entregado);
        respuesta.setNoEntregado(noEntregado);
        respuesta.setError(false);
		return respuesta;
	}
}
