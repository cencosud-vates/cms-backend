package com.cencosud.sim.util;

import java.util.Properties;
import org.apache.log4j.Logger;
import com.cencosud.sim.constants.Constants;

public class LeerProperties {
	
	static final Logger log = Logger.getLogger(LeerProperties.class);
	
	/**
	 * Obtiene en valor del archivo properties statictext.properties con la clave de b&uacute;squeda
	 * @param key
	 * @return
	 */
	public static String readStaticText(String key){
		
		Properties properties = null;
		String respuesta = null;
		try {
			properties = new Properties();
			properties.load(LeerProperties.class.getClassLoader().getResourceAsStream(Constants.STATIC_TEXT_PROPERTIES_FILE));
			respuesta = properties.getProperty(key);
		} catch (Exception e) {
			log.error(e);
		}
		return respuesta;
	}

	private LeerProperties() {
		super();
	}
	
}
