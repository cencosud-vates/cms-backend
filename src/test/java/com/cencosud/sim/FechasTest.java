package com.cencosud.sim;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.cencosud.sim.service.impl.FileUploadServiceImpl;

public class FechasTest {
	static final Logger log = Logger.getLogger(FechasTest.class);
	
	@Test
	public void probarFecha() {
		
		log.info(obtenerDate("02/31/2017"));
	}
	
	private Date obtenerDate(String fecha) {
		Date respuesta = null;
		if(fecha instanceof String && fecha!=null) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			try {
				respuesta = formatter.parse(fecha);
			} catch (ParseException e) {
				log.error(e);
			}
		}
		return respuesta;
	}

}
